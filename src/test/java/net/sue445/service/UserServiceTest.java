package net.sue445.service;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;

import net.sue445.entity.User;

import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.seasar.framework.unit.Seasar2;

@RunWith(Enclosed.class)
public class UserServiceTest {

	@RunWith(Seasar2.class)
	public static class Exists2Records {
		private UserService userService;

		@Test
		public void _2件取得できること() throws Exception {
			assertThat(userService.getCount(), is(2L));

			List<User> actual = userService.findAll();
			assertThat(actual.get(0).name, is("Ichiro"));
			assertThat(actual.get(1).name, is("Jiro"));
		}

		@Test
		public void _1件追加できる() throws Exception {
			User user = new User();
			user.name = "Saburou";
			userService.insert(user);

			assertThat(userService.getCount(), is(3L));
		}
	}

	@RunWith(Seasar2.class)
	public static class ExistsNoRecords {
		private UserService userService;

		@Test
		public void _0件取得できること() throws Exception {
			assertThat(userService.getCount(), is(0L));
		}

		@Test
		public void _1件追加できる() throws Exception {
			User user = new User();
			user.name = "Shirou";
			userService.insert(user);

			assertThat(userService.getCount(), is(1L));
		}
	}

}
